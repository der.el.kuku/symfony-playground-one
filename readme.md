# Symfony Playground "one"

[![Build Status](https://www.travis-ci.com/elkuku/symfony-playground-one.svg?branch=master)](https://www.travis-ci.com/elkuku/symfony-playground-one)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/elkuku/symfony-playground-one/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/elkuku/symfony-playground-one/?branch=master)

![screen](https://user-images.githubusercontent.com/33978/154263165-67bbd7e0-7778-443b-8e85-3184f759fddb.png)

## What's this??
An opinionated [Symfony](https://symfony.com) project template including:

* Symfony 6.*
* Docker compose file for PostgreSQL
* `dev` login form <br/> `prod` Social login with Google or GitHub (and [more](https://github.com/knpuniversity/oauth2-client-bundle#step-1-download-the-client-library))
* JQuery, Bootstrap and [Bootswatch](https://bootswatch.com/)
* [EasyAdmin](https://github.com/EasyCorp/EasyAdminBundle)
* Likes PHP 8 ;)

## Installation

Clone the repo then use the `bin/install` command or execute the script content manually.
   
## Usage

* Use `symfony console user-admin` to create an admin user.
* Use the `bin/start` and `bin/stop` scripts to start and stop the environment.

## Testing

```shell
make tests
```

----

Happy coding `=;)`
